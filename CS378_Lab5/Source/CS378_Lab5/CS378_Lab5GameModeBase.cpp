// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab5GameModeBase.h"
#include "Lab5Character.h"
#include "Lab5PlayerController.h"

ACS378_Lab5GameModeBase::ACS378_Lab5GameModeBase() {

	PlayerControllerClass = ALab5PlayerController::StaticClass();

	// set default pawn class to character class
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/StarterContent/Blueprints/Lab5CharacterBP.Lab5CharacterBP_C'"));

	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else
	{
		DefaultPawnClass = ALab5Character::StaticClass();
	}
};
