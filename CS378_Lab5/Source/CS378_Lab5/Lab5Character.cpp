// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab5Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ALab5Character::ALab5Character()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InteractionLength = 3.0f;

	maxWalkSpeed = 100.f;
	maxRunSpeed = 600.f;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 400.0f;
	CameraBoom->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;

	CameraComponent = CreateAbstractDefaultSubobject<UCameraComponent>(TEXT("OverTheShoulder"));
	CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;

	GetCharacterMovement()->MaxWalkSpeed = maxRunSpeed;
}

// Called when the game starts or when spawned
void ALab5Character::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ALab5Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab5Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ALab5Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	switch (CharacterActionState)
	{
	case ECharacterActionStateEnum::IDLE:
		return true;
		break;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction != ECharacterActionStateEnum::INTERACT) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::IDLE || updatedAction == ECharacterActionStateEnum::MOVE) {
			return true;
		}
		break;
	case ECharacterActionStateEnum::INTERACT:
		return false;
		break;
	}
	return false;
}

void ALab5Character::ApplyMovement(float scale)
{
	AddMovementInput(GetActorForwardVector(), scale);
}

void ALab5Character::ApplyStrafe(float scale)
{
	AddMovementInput(GetActorRightVector(), scale);
}

void ALab5Character::BeginInteraction()
{

	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Interaction Started"));
	GetWorld()->GetTimerManager().SetTimer(InteractionTimerHandle, this, &ALab5Character::EndInteraction, InteractionLength);
}

void ALab5Character::EndInteraction()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Interaction Ended"));
	UpdateActionState(ECharacterActionStateEnum::IDLE);
}

void ALab5Character::UpdateActionState(ECharacterActionStateEnum newAction)
{
	if (newAction == ECharacterActionStateEnum::MOVE || newAction == ECharacterActionStateEnum::IDLE) {
		if (FMath::Abs(GetVelocity().Size()) <= 0.01f) {
			CharacterActionState = ECharacterActionStateEnum::IDLE;
		}
		else
		{
			CharacterActionState = ECharacterActionStateEnum::MOVE;
		}
	}
	else
	{
		CharacterActionState = newAction;
	}
}

void ALab5Character::ToggleWalkSpeed() {
	if (GetCharacterMovement()->MaxWalkSpeed == maxRunSpeed)
	{
		GetCharacterMovement()->MaxWalkSpeed = maxRunSpeed;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = maxWalkSpeed;
	}
}