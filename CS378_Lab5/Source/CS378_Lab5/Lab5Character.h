// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TimerManager.h"
#include "Lab5Character.generated.h"


UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8 {
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class CS378_LAB5_API ALab5Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab5Character();
	FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UFUNCTION(BlueprintImplementableEvent)
		void Move(float value);

	UFUNCTION(BlueprintImplementableEvent)
		void Strafe(float value);

	UFUNCTION(BlueprintImplementableEvent)
		void InteractionStarted();

	UFUNCTION(BlueprintImplementableEvent)
		void InteractionEnded();

	UFUNCTION(BlueprintImplementableEvent)
		void JumpStarted();

	UFUNCTION(BlueprintCallable)
		bool CanPerformAction(ECharacterActionStateEnum updatedAction);

	UFUNCTION(BlueprintCallable)
		void ApplyMovement(float scale);

	UFUNCTION(BlueprintCallable)
		void ApplyStrafe(float scale);

	UFUNCTION(BlueprintCallable)
		void BeginInteraction();

	UFUNCTION(BlueprintCallable)
		void EndInteraction();

	UFUNCTION(BlueprintCallable)
		void UpdateActionState(ECharacterActionStateEnum newAction);

	UFUNCTION(BluePrintCallable)
		void ToggleWalkSpeed();

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ECharacterActionStateEnum CharacterActionState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float InteractionLength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float maxWalkSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float maxRunSpeed;

	UPROPERTY(Category = Camera, EditAnywhere, BlueprintReadWrite)
		class UCameraComponent * CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USpringArmComponent* CameraBoom;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	FTimerHandle InteractionTimerHandle;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
