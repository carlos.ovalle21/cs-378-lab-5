// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab5PlayerController.h"
#include "Lab5Character.h"

ALab5PlayerController::ALab5PlayerController()
{

}

void ALab5PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("Move", this, &ALab5PlayerController::move);
	InputComponent->BindAxis("Strafe", this, &ALab5PlayerController::strafe);
	InputComponent->BindAction("Interact", IE_Pressed, this, &ALab5PlayerController::interactStarted);
	InputComponent->BindAction("Interact", IE_Released, this, &ALab5PlayerController::interactEnded);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ALab5PlayerController::jumpStarted);

}

void ALab5PlayerController::move(float value)
{
	ALab5Character* character = Cast<ALab5Character>(this->GetCharacter());

	if (character) {
		character->Move(value);
	}
}

void ALab5PlayerController::strafe(float value)
{
	ALab5Character* character = Cast<ALab5Character>(this->GetCharacter());

	if (character) {
		character->Strafe(value);
	}
}

void ALab5PlayerController::interactStarted()
{
	ALab5Character* character = Cast<ALab5Character>(this->GetCharacter());

	if (character) {
		character->InteractionStarted();
	}
}

void ALab5PlayerController::interactEnded()
{
	ALab5Character* character = Cast<ALab5Character>(this->GetCharacter());

	if (character) {
		character->InteractionEnded();
	}
}

void ALab5PlayerController::jumpStarted()
{
	ALab5Character* character = Cast<ALab5Character>(this->GetCharacter());

	if (character) {
		character->JumpStarted();
	}
}