// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab5PlayerController.generated.h"

/**
 *
 */
UCLASS()
class CS378_LAB5_API ALab5PlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	ALab5PlayerController();

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
		void move(float value);
	UFUNCTION()
		void strafe(float value);
	UFUNCTION()
		void interactStarted();
	UFUNCTION()
		void interactEnded();
	UFUNCTION()
		void jumpStarted();
};
